using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class CheckerToShuffle : MonoBehaviour
{
    private async void OnTriggerEnter(Collider other)
    {
        var tileHolder = other.transform;
        if (tileHolder.CompareTag("TileHolder") && (int)tileHolder.gameObject.layer == 0)
        {
            if (tileHolder.GetComponent<TilesHolder>() != null)
            {
                if (tileHolder.GetComponent<TilesHolder>().enabled == false)
                {
                    tileHolder.transform.DOScale(Vector3.zero, 0.2f);
                    await Task.Delay(210);
                    if (tileHolder != null)
                        Destroy(tileHolder.gameObject);
                }
            }
        }
    }
}