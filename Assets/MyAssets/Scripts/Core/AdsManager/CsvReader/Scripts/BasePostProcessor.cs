﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Zitga.CsvTools;

#if UNITY_EDITOR

public class CodeGeneratorPathUtils
{
    private const string ScriptPath = "/_Core/Scripts/";
    private const string DefaultClassName = "PathUtils";
    string template = "public class ${name}\r\n{${format}}";
    string format = "\npublic const string {0} = \"{1}\";";

    public string Generate()
    {
        StringBuilder propertiesBuf = new StringBuilder();

        var define = Resources.Load<DefineCollection>("Collection/define_collection");
        if (define == null) return "";

        foreach (var kv in define.dataGroups)
        {
            var paths = kv.assetPath.Split("/");
            if (paths.Length > 0)
            {
                var id = paths[^1];
                for (int z = 0; z < id.Length; z++) // or count if it is a list
                {
                    if (id[z] == '_')
                    {
                        var index = z;
                        if (index < id.Length - 1)
                        {
                            id = id.Replace("_" + id[index + 1], id[index + 1].ToString().ToUpper());
                        }
                        else
                        {
                            id = id.Replace("_", "");
                        }
                    }
                }
                
                propertiesBuf.Append(SetUpClass(id, kv.assetPath));
            }
        }

        var code = template.Replace("${name}", DefaultClassName);
        code = code.Replace("${format}", propertiesBuf.ToString());
        return code;
    }

    private string SetUpClass(string format0, string format1)
    {
        var code = string.Format(format, format0, format1);
        return code;
    }

    private string GetPropertyName(string key)
    {
        return Regex.Replace(key, "[.]", "_");
    }

    public void WriteFile(string code)
    {
        // path to write code
        var writeFolder = Application.dataPath + ScriptPath;
        if (!Directory.Exists(writeFolder))
            Directory.CreateDirectory(writeFolder);

        File.WriteAllText(writeFolder + DefaultClassName + ".cs", code);
    }

    public void GenFullProcess()
    {
        var code = Generate();

        WriteFile(code);
    }
}

public class BasePostProcessor : AssetPostprocessor
{
    public const string CSV_PATH = "Assets/Csv/";
    private static CodeGeneratorPathUtils pathUtils;

    public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
        string[] movedFromAssetPaths)

    {
        if (pathUtils == null) pathUtils = new CodeGeneratorPathUtils();
        string[] asset;
        if (importedAssets.Length > 0)
        {
            asset = importedAssets[0].Split('/');
        }
        else
            return;

        var data = asset[asset.Length - 1];
        if (data.Contains(".csv"))
            Setup(data, importedAssets);
    }

    static void Setup(string path, string[] importedAssets)
    {
        Action<string, TextAsset, Type, Type> genAsset =
            delegate(string assetfile, TextAsset data, Type classCollection, Type classData)
            {
                var gm = AssetDatabase.LoadAssetAtPath(assetfile, classCollection);

                if (gm == null)
                {
                    gm = ScriptableObject.CreateInstance(classCollection);
                    if (gm == null)
                    {
                        Debug.LogError("class null: "+classCollection);
                        return;
                    }
                    AssetDatabase.CreateAsset(gm, assetfile);
                }

                Type type = classData;
                var field = gm.GetType().GetField("dataGroups",
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                var method = gm.GetType().GetMethod("Convert");

                if (field != null)
                {
                    //Debug.LogError(assetfile);
                    var value = CsvReader.Deserialize(data.text, type, assetfile);
                    try
                    {
                        field.SetValue(gm, value);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogWarning("Chỉ có một phần tử, cần sửa lại cấu trúc data của: " + type);
                        return;
                    }

                    method?.Invoke(gm, null);

                    if (field.IsPrivate) field.SetValue(gm, null);
                }

                EditorUtility.SetDirty(gm);
                AssetDatabase.SaveAssets();
            };

        foreach (string str in importedAssets)
        {
            if (str.IndexOf(path) != -1)
            {
//                Debug.Log("str: " + str);
                TextAsset data = AssetDatabase.LoadAssetAtPath<TextAsset>(str);

                var isDefineCollection = path.Equals("define_collection.csv");

                var assetfile = "";
                Type classCollection;
                Type classData;

                if (isDefineCollection)
                {
                    // Load define collection first
                    if (!Directory.Exists("Assets/Resources/Collection"))
                    {
                        Directory.CreateDirectory("Assets/Resources/Collection");
                    }

                    assetfile = "Assets/Resources/Collection/define_collection.asset";
                    classCollection = Type.GetType("DefineCollection");
                    classData = Type.GetType("DefineData");
                    genAsset(assetfile, data, classCollection, classData);
                }
                else
                {
                    var defineCollection =
                        Resources.Load<DefineCollection>("Collection/define_collection");


                    // check define collection, if null, will reload
                    if (defineCollection == null)
                    {
                        TextAsset textAsset =
                            AssetDatabase.LoadAssetAtPath<TextAsset>(
                                CSV_PATH + "define_collection.csv");

                        Debug.Log("textAsset:" + textAsset);
                        assetfile = "Assets/Resources/Collection/define_collection.asset";
                        classCollection = Type.GetType("DefineCollection");
                        classData = Type.GetType("DefineData");
                        genAsset(assetfile, textAsset, classCollection, classData);
                    }

                    // check define collection again
                    defineCollection =
                        Resources.Load<DefineCollection>("Collection/define_collection");
                    var defineData = defineCollection.GetDefineCollectionData(path);
                    if (defineData == null) break;

                    var pathDefineData = defineData.assetPath;
                    var strIndex = pathDefineData.Split('/');

                    if (strIndex.Length > 1)
                    {
                        for (int i = 0; i < strIndex.Length - 1; i++)
                        {
                            if (i == strIndex.Length - 2)
                            {
                                assetfile += strIndex[i];
                            }
                            else
                            {
                                assetfile += strIndex[i] + "/";
                            }
                        }

                        assetfile = "Assets/Resources/" + assetfile;

                        if (!Directory.Exists(assetfile))
                            Directory.CreateDirectory(assetfile);
                    }

                    // gen asset
                    assetfile = "Assets/Resources/" + defineData.assetPath + ".asset";

                    classCollection = Type.GetType(defineData.classCollection);
                    classData = Type.GetType(defineData.classData);
                    genAsset(assetfile, data, classCollection, classData);
                }

#if DEBUG_LOG || UNITY_EDITOR
                Debug.Log("Reimported Asset: " + assetfile);
#endif
            }
        }
        
        pathUtils.GenFullProcess();
    }
}
#endif