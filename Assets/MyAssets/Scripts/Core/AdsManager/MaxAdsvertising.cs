using System;
using UnityEngine;

public class MaxAdsvertising : IAdvertising, IRemoteConfigAdvertising
{
    private bool isInited;

    public string AppKey => MediationConstant.Max.SdkKey;

    public bool IsShowReward { get; set; }
    public bool CanShowInterstitial { get; set; }
    public int CountTimeShowInterstitialAds { get; set; }
    public int TimeDelayShowInterstitialAds { get; set; }
    public bool IsShowInterstitialAds { get; set; }


    public void InitEvent()
    {
        if (isInited)
        {
            return;
        }


        #region Reward Video

        // Attach callback
        MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdLoadFailedEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEventRewarded;
        MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdHiddenEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        // Load the first rewarded ad
        LoadRewardAds();

        #endregion

        #region Interstital

        // Attach callback

        MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialLoadFailedEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialDisplayedEvent;
        MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
        MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialHiddenEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += OnInterstitialAdFailedToDisplayEvent;
        MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEventInter;


        // Load the first interstitial
        LoadInterstitial();

        #endregion

        #region Banner

        // Banners are automatically sized to 320×50 on phones and 728×90 on tablets
        // You may call the utility method MaxSdkUtils.isTablet() to help with view sizing adjustments
        MaxSdk.CreateBanner(MediationConstant.Max.BannerStringId, MaxSdkBase.BannerPosition.TopCenter);
        MaxSdk.SetExtraParameter("adaptive_banner", "false");
        // Set background or background color for banners to be fully functional
        MaxSdk.SetBannerBackgroundColor(MediationConstant.Max.BannerStringId, new Color(255, 255, 255, 0));

        MaxSdkCallbacks.Banner.OnAdLoadedEvent += OnBannerAdLoadedEvent;
        MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += OnBannerAdLoadFailedEvent;
        MaxSdkCallbacks.Banner.OnAdClickedEvent += OnBannerAdClickedEvent;
        MaxSdkCallbacks.Banner.OnAdExpandedEvent += OnBannerAdExpandedEvent;
        MaxSdkCallbacks.Banner.OnAdCollapsedEvent += OnBannerAdCollapsedEvent;
        MaxSdkCallbacks.Banner.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEventBanner;

        #endregion

        #region MREC

        #endregion

        isInited = true;
    }

    public void InitAds()
    {
        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => { InitEvent(); };

        MaxSdk.SetSdkKey(AppKey);
        //MaxSdk.SetUserId("USER_ID");
        MaxSdk.InitializeSdk();
        //InitializeMRecAds();
        //MaxSdk.LoadMRec(MediationConstant.Max.Mrec);
    }

    public bool IsReadyVideoAds()
    {
        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork ||
            Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            return MaxSdk.IsRewardedAdReady(MediationConstant.Max.RewardedStringId);
        }

        return false;
    }

    public void LoadRewardAds()
    {
        MaxSdk.LoadRewardedAd(MediationConstant.Max.RewardedStringId);
    }

    public enum AdsType
    {
        interstitial = 0,
        reward = 1,
        banner
    }

    private Action<bool> callback;
    private string source;
    private string adsType;
    private bool isRewarded;

    public void ShowRewardVideo(Action<bool> onFinish = null, Action cb = null, Action fail = null,
        string source = null)
    {
        adsType = AdsType.reward.ToString();
        isRewarded = false;
        this.source = source;

#if UNITY_EDITOR
        onFinish?.Invoke(true);
        return;
#endif

        callback = onFinish;
        AppsFlyerEventAdsEligible();
        if (IsReadyVideoAds())
        {
            AppsFlyerEventAdsCalled();
            MaxSdk.ShowRewardedAd(MediationConstant.Max.RewardedStringId, source);
        }
        else
        {
            Debug.LogError("Reload ads");
            LoadRewardAds();
        }
    }

    public bool IsInterstitialReady()
    {
        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork ||
            Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            return MaxSdk.IsInterstitialReady(MediationConstant.Max.InterstitialStringId);
        }

        return false;
    }

    public void LoadInterstitial()
    {
        MaxSdk.LoadInterstitial(MediationConstant.Max.InterstitialStringId);
    }

    private Action finishVideo;
    private Action failVideo;

    public void ShowInterstitial(Action onFinish = null, Action onClose = null, Action onFail = null,
        string source = null)
    {
        Debug.Log("Show Inter");
        this.finishVideo = onFinish;
        this.failVideo = onFail;
        adsType = AdsType.interstitial.ToString();
        if (IsInterstitialReady())
        {
            AdsManager.Instance.canShowInter = false;
            AdsManager.Instance.count = 0;
            finishVideo = onFinish;
            failVideo = onFail;
            //CommonTracking.SendEventAF(CommonEventTracking.af_inters_available);
            MaxSdk.ShowInterstitial(MediationConstant.Max.InterstitialStringId, source);
        }
        else
        {
            onFail?.Invoke();
            //Debug.Log("unity-script: IronSource.Agent.isInterstitialReady - False");
            LoadInterstitial();
        }
    }

    public void ShowBannerAds()
    {
        MaxSdk.ShowBanner(MediationConstant.Max.BannerStringId);
        MaxSdk.UpdateBannerPosition(MediationConstant.Max.BannerStringId, MaxSdkBase.BannerPosition.BottomCenter);
    }

    public void HideBannerAds()
    {
        MaxSdk.HideBanner(MediationConstant.Max.BannerStringId);
    }


    public void HideMRec()
    {
        //MaxSdk.HideMRec(MediationConstant.Max.Mrec);
    }

    public void InitializeMRecAds()
    {
    }


    public void LoadMrec()
    {
    }

    public bool CanShowInter()
    {
        return true;
    }

    public void CreateMRec(Vector2 pos)
    {
    }

    public void ShowMRec()
    {
    }


    public void ToggleMRecVisibility()
    {
    }

    public void OnApplicationPause(bool isPause)
    {
    }


    private void AppsFlyerEventAdsEligible()
    {
        try
        {
        }
        catch (Exception e)
        {
        }
    }

    private void AppsFlyerEventAdsCalled()
    {
        try
        {
        }
        catch (Exception e)
        {
        }
    }

    private void AppsFlyerEventAdsDisplayed()
    {
        try
        {
        }
        catch (Exception e)
        {
        }
    }


    // ------------------------------------------- REWARD ADS ----------------------------------------------------
    private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad is ready for you to show. MaxSdk.IsRewardedAdReady(adUnitId) now returns 'true'.

        // Reset retry attempt
    }

    private async void OnRewardedAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        // Rewarded ad failed to load 
        // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds).
        LoadRewardAds();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        //TrackingManagerExtension.PushEvent(EventTracking.ads_reward_show, source);
        AppsFlyerEventAdsDisplayed();
        // EventManager.EmitEventData(EventName.Ads.StartAds, source);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
        MaxSdkBase.AdInfo adInfo)
    {
        LoadRewardAds();
    }

    private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        //if (!isRewarded)
        //SN.ShowNotification(
        //new ShortNotificationProperties(Localization.Current.Get("common", "not_rewarded_ads")));
    }

    private void OnRewardedAdHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Rewarded ad is hidden. Pre-load the next ad
        LoadRewardAds();
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
    {
        isRewarded = true;
        //TrackingManagerExtension.PushEvent(EventTracking.ads_reward_complete, source);

        var placement = source;
        //CommonTracking.AdsReward(placement);
        //AdsPowerService.AddWatchAds();
        //PublisherService.NotifyListener(SubjectType.WatchAds);

        callback?.Invoke(true);

        //EventManager.EmitEvent(EventName.Ads.FinishAds);
    }

    private void OnRewardedAdRevenuePaidEventBanner(string adUnitId, MaxSdkBase.AdInfo impressionData)
    {
        try
        {
            double revenue = impressionData.Revenue;
            var impressionParameters = new[]
            {
                new Firebase.Analytics.Parameter("ad_platform", "AppLovin"),
                new Firebase.Analytics.Parameter("ad_source", impressionData.NetworkName),
                new Firebase.Analytics.Parameter("ad_unit_name", impressionData.AdUnitIdentifier),
                new Firebase.Analytics.Parameter("ad_format", impressionData.Placement),
                new Firebase.Analytics.Parameter("value", revenue),
                new Firebase.Analytics.Parameter("currency", "USD"), // All Applovin revenue is sent in USD
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression_banner", impressionParameters);
        }
        catch
        {
        }
        //CommonTracking.AppsFlyerAdsCompleted(adsType, impressionData.NetworkName, source, revenue);
    }

    private void OnRewardedAdRevenuePaidEventRewarded(string adUnitId, MaxSdkBase.AdInfo impressionData)
    {
        try
        {
            double revenue = impressionData.Revenue;
            var impressionParameters = new[]
            {
                new Firebase.Analytics.Parameter("ad_platform", "AppLovin"),
                new Firebase.Analytics.Parameter("ad_source", impressionData.NetworkName),
                new Firebase.Analytics.Parameter("ad_unit_name", impressionData.AdUnitIdentifier),
                new Firebase.Analytics.Parameter("ad_format", impressionData.Placement),
                new Firebase.Analytics.Parameter("value", revenue),
                new Firebase.Analytics.Parameter("currency", "USD"), // All Applovin revenue is sent in USD
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression_rewarded", impressionParameters);
            //CommonTracking.AppsFlyerAdsCompleted(adsType, impressionData.NetworkName, source, revenue);
        }
        catch
        {
        }
    }

    private void OnRewardedAdRevenuePaidEventInter(string adUnitId, MaxSdkBase.AdInfo impressionData)
    {
        try
        {
            double revenue = impressionData.Revenue;
            var impressionParameters = new[]
            {
                new Firebase.Analytics.Parameter("ad_platform", "AppLovin"),
                new Firebase.Analytics.Parameter("ad_source", impressionData.NetworkName),
                new Firebase.Analytics.Parameter("ad_unit_name", impressionData.AdUnitIdentifier),
                new Firebase.Analytics.Parameter("ad_format", impressionData.Placement),
                new Firebase.Analytics.Parameter("value", revenue),
                new Firebase.Analytics.Parameter("currency", "USD"), // All Applovin revenue is sent in USD
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression_inter", impressionParameters);
        }
        catch
        {
        }
        //CommonTracking.AppsFlyerAdsCompleted(adsType, impressionData.NetworkName, source, revenue);
    }

    // ------------------------------------------- Interstitial ADS ----------------------------------------------------
    private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        // Interstitial ad is ready for you to show. MaxSdk.IsInterstitialReady(adUnitId) now returns 'true'
        //AppsFlyer.sendEvent(AFInAppEvents.INTER_ADS_API_CALLED, null);
        //FirebaseManager.Instance.LogLoadInterSuccess();
        // Reset retry attempt
        //_interstitialsRetryTime = 0;
    }

    private async void OnInterstitialLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        LoadInterstitial();
    }

    private void OnInterstitialDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        CountTimeShowInterstitialAds = 0;
        CanShowInterstitial = false;
    }

    private void OnInterstitialAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
        MaxSdkBase.AdInfo adInfo)
    {
        failVideo?.Invoke();
        // Interstitial ad failed to display. AppLovin recommends that you load the next ad.
        //FirebaseManager.Instance.LogShowInterFail(errorInfo.Message);
        LoadInterstitial();
    }

    private void OnInterstitialClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        //FirebaseManager.Instance.LogClickInter();
    }

    private void OnInterstitialHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        finishVideo?.Invoke();
        //EventManager.EmitEvent(EventName.Ads.ShowInter);
        // Interstitial ad is hidden. Pre-load the next ad.
        LoadInterstitial();
    }

    // ------------------------------------------- Banner ADS ----------------------------------------------------
    private void OnBannerAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        //EventManager.EmitEvent(NameConstants.EventName.LoadedBanner);
    }

    private void OnBannerAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
    }

    private void OnBannerAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
    }

    private void OnBannerAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
    }

    private void OnBannerAdExpandedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
    }

    private void OnBannerAdCollapsedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
    }
}