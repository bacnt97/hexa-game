using Firebase.Analytics;
using System;
using System.Collections.Generic;
using System.Reflection;
using AppsFlyerSDK;
using Firebase.Extensions;
using UnityEngine;

public static class GameConstant
{
    public static int NULL_NUMBER = -9999999;
}
public enum FirebaseEvent
{
    ads_reward_click,
    ads_reward_show,
    ads_reward_completed,
    ads_reward_fail,
    
    ads_inter_click,
    ads_inter_show,
    ads_inter_completed,
    ads_inter_fail,
}

public class FirebaseEventConfig
{
    public int level = GameConstant.NULL_NUMBER;
}
public static class TrackingService
{
    private static bool CanTracking;

    public static void Init()
    {
        AppsFlyer.initSDK("ffuztcNbmPxymeeErLjKuQ", null);
        Debug.LogError("InitFirebase");
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available) {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                var app = Firebase.FirebaseApp.DefaultInstance;
                CanTracking = true;
                Debug.LogError("first_time");
                if (PlayerPrefs.GetInt("first_time", 0) == 0)
                {
                    PlayerPrefs.SetInt("first_time", 1);
                    Send("first_time_open");
                    Debug.LogError("on first_time");
                }
                // Set a flag here to indicate whether Firebase is ready to use by your app.
            } else {
                UnityEngine.Debug.LogError(System.String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }
        public static void Send(string eventName, FirebaseEventConfig config = null)
        {
            if (!CanTracking) return;
        try
        {
            if (config == null)
            {
                FirebaseAnalytics.LogEvent(eventName.ToString());
                Debug.LogError("Event name: " + eventName);
                return;
            }
            var firebaseEventConfigProperties = config.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            var parameters = new List<Parameter>();

            var param = "";

            foreach (var prop in firebaseEventConfigProperties)
            {
                var type = prop.FieldType;
                var value = prop.GetValue(config);
                if (value != null)
                {
                    if (type == typeof(string))
                    {
                        var lastValue = value as string;
                        if (!string.IsNullOrEmpty(lastValue))
                        {
                            parameters.Add(new Parameter(prop.Name, lastValue));
                        }
                    }
                    else if (type == typeof(float))
                    {
                        var lastValue = (float)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(double))
                    {
                        var lastValue = (double)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(long))
                    {
                        var lastValue = (long)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(int))
                    {
                        var lastValue = (int)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
#if UNITY_EDITOR
                    param += prop.Name + ": " + value + "\n";
#endif
                }
            }
#if UNITY_EDITOR
            Debug.Log("-----------FIREBASE TRACKING-----------");
            Debug.Log(eventName);
            Debug.Log(param);
#endif
            if (parameters == null || parameters.Count == 0)
            {
                FirebaseAnalytics.LogEvent(eventName.ToString());
            }
            else
            {
                foreach (var item in parameters)
                {
                    Debug.Log(item.ToString());
                }

                FirebaseAnalytics.LogEvent(eventName.ToString(), parameters.ToArray());
            }
        }
        catch (Exception e)
        {
            Debug.Log("-------------------EXCEPTION--------------------");
            Debug.Log("EXCEPTION: " + e);
            Debug.Log("Event name: " + eventName);
            Debug.Log("--------------------------------------------------");
            Debug.LogError(e);
        }
    }
        
    public static void Send(this FirebaseEvent eventName, FirebaseEventConfig config)
    {
        try
        {
            var firebaseEventConfigProperties = config.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            var parameters = new List<Parameter>();

            var param = "";

            foreach (var prop in firebaseEventConfigProperties)
            {
                var type = prop.FieldType;
                var value = prop.GetValue(config);
                if (value != null)
                {
                    if (type == typeof(string))
                    {
                        var lastValue = value as string;
                        if (!string.IsNullOrEmpty(lastValue))
                        {
                            parameters.Add(new Parameter(prop.Name, lastValue));
                        }
                    }
                    else if (type == typeof(float))
                    {
                        var lastValue = (float)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(double))
                    {
                        var lastValue = (double)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(long))
                    {
                        var lastValue = (long)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
                    else if (type == typeof(int))
                    {
                        var lastValue = (int)value;
                        if (lastValue == GameConstant.NULL_NUMBER) continue;
                        parameters.Add(new Parameter(prop.Name, lastValue));
                    }
#if UNITY_EDITOR
                    param += prop.Name + ": " + value + "\n";
#endif
                }
            }
            Debug.Log("-----------FIREBASE TRACKING-----------");
            Debug.Log(eventName);
            Debug.Log(param);
            if (parameters == null || parameters.Count == 0)
            {
                FirebaseAnalytics.LogEvent(eventName.ToString());
            }
            else
            {
                foreach (var item in parameters)
                {
                    Debug.Log(item.ToString());
                }

                FirebaseAnalytics.LogEvent(eventName.ToString(), parameters.ToArray());
            }
        }
        catch (Exception e)
        {
            Debug.Log("-------------------EXCEPTION--------------------");
            Debug.Log("EXCEPTION: " + e);
            Debug.Log("Event name: " + eventName);
            Debug.Log("--------------------------------------------------");
            Debug.LogError(e);
        }
    }
    //
    // public static void SetUserProperty(UserPropertyConfig config)
    // {
    //     try
    //     {
    //         var FirebaseEventConfigProperties = config.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
    //
    //         foreach (var prop in FirebaseEventConfigProperties)
    //         {
    //             var type = prop.FieldType;
    //             var value = prop.GetValue(config);
    //             var stringValue = "";
    //             if (value != null)
    //             {
    //                 if (type == typeof(string))
    //                 {
    //                     var lastValue = value as string;
    //                     if (!string.IsNullOrEmpty(lastValue))
    //                     {
    //                         stringValue = lastValue;
    //                     }
    //                 }
    //                 else if (type == typeof(float))
    //                 {
    //                     var lastValue = (float)value;
    //                     if (lastValue == GameConstant.NULL_NUMBER) continue;
    //                     stringValue = lastValue.ToString();
    //                 }
    //                 else if (type == typeof(double))
    //                 {
    //                     var lastValue = (double)value;
    //                     if (lastValue == GameConstant.NULL_NUMBER) continue;
    //                     stringValue = lastValue.ToString();
    //                 }
    //                 else if (type == typeof(long))
    //                 {
    //                     var lastValue = (long)value;
    //                     if (lastValue == GameConstant.NULL_NUMBER) continue;
    //                     stringValue = lastValue.ToString();
    //                 }
    //                 else if (type == typeof(int))
    //                 {
    //                     var lastValue = (int)value;
    //                     if (lastValue == GameConstant.NULL_NUMBER) continue;
    //                     stringValue = lastValue.ToString();
    //                 }
    //                 Logger.Info("-----------------------SET PROPERTY--------------------------");
    //                 Logger.Info(prop.Name + "|-----------|" + value);
    //                 Logger.Info("------------------------------------------------------------");
    //                 FirebaseAnalytics.SetUserProperty(prop.Name, stringValue);
    //             }
    //         }
    //     }
    //     catch
    //     {
    //     }
    // }
    //
    // public static void Send(this AppFlyerEvent eventName, AppflyerEventConfig config)
    // {
    //     try
    //     {
    //         var FirebaseEventConfigProperties = config.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
    //         var dict = new Dictionary<string, string>();
    //
    //         foreach (var prop in FirebaseEventConfigProperties)
    //         {
    //             var value = prop.GetValue(config) as string;
    //             if (!string.IsNullOrEmpty(value))
    //                 dict.Add(prop.Name, value);
    //         }
    //
    //         AppsFlyer.sendEvent(eventName.ToString(), dict);
    //     }
    //     catch
    //     {
    //     }
    // }
}