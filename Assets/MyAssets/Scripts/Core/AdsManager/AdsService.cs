using System;
using UnityEngine;

public static class AdsService
{
    public static bool CanShowInter()
    {
        if (AdsManager.Instance != null)
        {
            return AdsManager.Instance.CanShowInter();
        }

        return false;
    }

    public static void ShowBanner()
    {
    }

    public static void ShowInterstitial(Action onFinish = null, Action onClose = null, Action onFail = null,
        string source = null)
    {
#if UNITY_EDITOR
        onFinish?.Invoke();
#endif
        if (AdsManager.Instance != null)
            AdsManager.Instance.ShowInterstitial(onFinish, onClose, onFail, source);
    }

    public static void ShowRewardedVideo(Action<bool> cb = null)
    {
#if UNITY_EDITOR
        cb?.Invoke(true);
#endif
        if (AdsManager.Instance != null)
        {
            Debug.LogError("Show Reward 1");
            AdsManager.Instance.ShowRewardedVideo(cb);
        }
    }

    public static bool IsRewardedVideoAvailable()
    {
        return true;
    }
}