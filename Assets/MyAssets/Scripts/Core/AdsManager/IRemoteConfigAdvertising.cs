using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRemoteConfigAdvertising
{
    /// <summary>
    /// Có đang hiển thị reward không
    /// </summary>
    bool IsShowReward { get; set; }
    
    /// <summary>
    /// Biến để kiểm tra trạng thái inter có thể show được không
    /// </summary>
    bool CanShowInterstitial { get; set; }
    
    /// <summary>
    /// Thời gian đếm khi show inter
    /// </summary>
    int CountTimeShowInterstitialAds { get; set; }
    
    /// <summary>
    /// Thời gian giữa 2 show inter
    /// </summary>
    int TimeDelayShowInterstitialAds { get; set; }
    
    /// <summary>
    /// Biến bật tắt show inter bằng remote  config
    /// </summary>
    bool IsShowInterstitialAds { get; set; }
}
