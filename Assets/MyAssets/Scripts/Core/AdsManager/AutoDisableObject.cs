using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisableObject : MonoBehaviour
{
    public float time;

    private Coroutine corou;

    private void OnEnable()
    {
        if (corou != null)
        {
            StopCoroutine(corou);
        }

        corou = StartCoroutine(Corou());
    }

    IEnumerator Corou()
    {
        yield return new WaitForSeconds(time);
        if (this != null && gameObject != null)
            gameObject.SetActive(false);
    }
}