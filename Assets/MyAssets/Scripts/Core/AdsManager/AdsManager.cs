using System;
using System.Collections;
using Firebase.Extensions;
using Game.Scripts.Manager;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;
    public IAdvertising advertising;
    public IRemoteConfigAdvertising advertisingRemoteConfig;
    public Vector2 size;
    private bool canLoadBanner = false;
    public float intervalTime;
    public bool canShowInter;
    private void Awake()
    {
        Application.targetFrameRate = 120;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.runInBackground = true;
        
        if (Instance == null)
        {
            canShowInter = true;
            
            Instance = this;
            DontDestroyOnLoad(this);
            Init();
            StartCoroutine(Cooldown());
        }
        else
        {
            Destroy(this);
        }
    }

    public int count;
    IEnumerator Cooldown()
    {
        while (true)
        {
            count++;
            yield return new WaitForSeconds(1);
            if (count >= intervalTime)
            {
                canShowInter = true;
                count = 0;
            }
        }
    }
    protected void Init()
    {
        TrackingService.Init();
        AdsPolicyManager.Setup();
        advertising = new MaxAdsvertising();
        advertisingRemoteConfig = advertising as IRemoteConfigAdvertising;
        InitAds();
        advertising.InitEvent();
        LoadManualAds();
    }

    private void InitAds()
    {
        advertising.InitAds();
    }

    // public bool CanShowInter()
    // {
    //     return advertising.Can
    // }
    // private async Task ShowInterWheneverReady()
    // {
    //     while (!advertising.IsInterstitialReady())
    //     {
    //         await UniTaskService.DelayFrame(1);
    //     }
    //
    //     ShowInterstitial();
    // }


    #region Reward Ads

    private void IntervalTime()
    {
        advertisingRemoteConfig.CountTimeShowInterstitialAds++;
        if (advertisingRemoteConfig.CountTimeShowInterstitialAds >=
            advertisingRemoteConfig.TimeDelayShowInterstitialAds)
        {
            advertisingRemoteConfig.CanShowInterstitial = true;
        }

        LoadManualAds();
    }

    private void LoadManualAds()
    {
        advertising.LoadRewardAds();
        advertising.LoadInterstitial();
        advertising.ShowBannerAds();
    }

    public bool IsVideoRewardReady()
    {
        return advertising.IsReadyVideoAds();
    }

    public void ShowRewardedVideo(Action<bool> onFinish = null)
    {
        advertising.ShowRewardVideo(onFinish);
    }

    #endregion

    public bool CanShowInter()
    {
        return advertising.CanShowInter() && canShowInter;
    }

    #region Interstitial Ads

    public bool IsInterstitialReady()
    {
        return advertising.IsInterstitialReady();
    }

    public void LoadInterstitial()
    {
        if (!IsInterstitialReady())
        {
            advertising.LoadInterstitial();
        }
    }

    public void ShowInterstitial(Action onFinish = null, Action onClose = null, Action onFail = null,
        string source = null)
    {
#if UNITY_EDITOR
        //GoogleAdMobController.Instance.OnAdInterClosedEvent?.Invoke();
        Debug.Log("Show Inter");
        count = 0;
        canShowInter = false;
        onFinish?.Invoke();
        return;
#endif
        advertising.ShowInterstitial(onFinish, onClose, onFail, source);
    }

    #endregion

    #region Banner Ads

    public void LoadBanner()
    {
    }

    public void HideBanner()
    {
        advertising.HideBannerAds();
    }

    #endregion


    #region Mrec

    public void CreateMRec(Vector2 pos)
    {
        advertising.CreateMRec(pos);
    }

    public void ShowMrec()
    {
        advertising.ShowMRec();
    }

    public void HideMrec()
    {
        advertising.HideMRec();
    }

    #endregion

    void OnApplicationPause(bool isPaused)
    {
        if (advertising != null)
        {
            advertising.OnApplicationPause(isPaused);
        }
    }
}