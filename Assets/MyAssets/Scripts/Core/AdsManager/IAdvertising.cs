using System;
using UnityEngine;

public interface IAdvertising
{
    /// <summary>
    /// Key của mediation
    /// </summary>
    string AppKey { get; }

    /// <summary>
    /// Khởi tạo các event của ads inter, banner, reward,...
    /// </summary>
    void InitEvent();

    /// <summary>
    /// Khởi tạo sdk mediation
    /// </summary>
    void InitAds();

    /// <summary>
    /// Kiểm tra xem video ads có sẵn sàng không
    /// </summary>
    /// <returns></returns>
    bool IsReadyVideoAds();

    /// <summary>
    /// Load reward
    /// </summary>
    void LoadRewardAds();

    /// <summary>
    /// Show reward ads
    /// </summary>
    /// <param name="onFinish"></param>
    /// <param name="onClose"></param>
    /// <param name="onFail"></param>
    /// <param name="source"></param>
    void ShowRewardVideo(Action<bool> onFinish = null, Action onClose = null, Action onFail = null, string source = null);

    /// <summary>
    /// Kiểm tra xem inter đã sẵn sàng chưa
    /// </summary>
    /// <returns></returns>
    bool IsInterstitialReady();

    /// <summary>
    /// Load inter
    /// </summary>
    void LoadInterstitial();


    /// <summary>
    /// Create Mrec
    /// </summary>
    void CreateMRec(Vector2 pos);

    void LoadMrec();


    /// <summary>
    /// Show Mrec
    /// </summary>
    void ShowMRec();

    /// <summary>
    /// Hide Mrec
    /// </summary>
    void HideMRec();

    /// <summary>
    /// Show inter
    /// </summary>
    void ShowInterstitial(Action onFinish = null, Action onClose = null, Action onFail = null, string source = null);

    /// <summary>
    /// Show Banner
    /// </summary>
    void ShowBannerAds();

    /// <summary>
    /// Hide Banner
    /// </summary>
    void HideBannerAds();

    /// <summary>
    /// Event khi app pause
    /// </summary>
    /// <param name="isPause"></param>
    void OnApplicationPause(bool isPause);


    void InitializeMRecAds();

    void ToggleMRecVisibility();

    bool CanShowInter();

}
