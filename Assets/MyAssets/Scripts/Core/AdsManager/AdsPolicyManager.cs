﻿using UnityEngine;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

namespace Game.Scripts.Manager
{
    public static class AdsPolicyManager
    {
        public static void Setup()
        {
            if (Application.isEditor)
            {
                return;
            }

            SetupPolicy();
        }

#if IRON_SOURCE
        private static void SetupPolicy()
        {
            #region Iron Source

            // https://developers.is.com/ironsource-mobile/unity/regulation-advanced-settings
            IronSource.Agent.setConsent(true);
            IronSource.Agent.setMetaData("do_not_sell", "false");
            IronSource.Agent.setMetaData("is_child_directed", "false");

            #endregion

            #region Unity Ads

            // https://developers.is.com/ironsource-mobile/unity/unityads-mediation-guide/#step-5
            IronSource.Agent.setMetaData("UnityAds_coppa", "false");

            #endregion

            #region AdMob

            // https://developers.is.com/ironsource-mobile/unity/admob-mediation-guide/#step-6
            IronSource.Agent.setMetaData("AdMob_TFCD", "false");
            IronSource.Agent.setMetaData("AdMob_TFUA", "false");
            IronSource.Agent.setMetaData("AdMob_MaxContentRating", "MAX_AD_CONTENT_RATING_MA");

            #endregion

            #region Vungle

            // https://developers.is.com/ironsource-mobile/unity/liftoff-monetize-mediation-guide/#step-6
            // Liftoff is also called Vungle
            IronSource.Agent.setMetaData("Vungle_coppa", "false");

            #endregion

            #region Meta

            // https://developers.is.com/ironsource-mobile/unity/facebook-mediation-guide/#step-9
            IronSource.Agent.setMetaData("Meta_Mixed_Audience", "false");

            // https://developers.facebook.com/docs/marketing-apis/data-processing-options
            SetDataProcessingOptions(new string[] { });

            // https://developers.facebook.com/docs/app-events/guides/advertising-tracking-enabled
            SetAdvertiserTrackingEnabled(true);

            #endregion

            #region Mintegral

            // https://developers.is.com/ironsource-mobile/unity/mintegral-integration-guide/#step-7
            IronSource.Agent.setMetaData("Mintegral_COPPA", "false");

            #endregion

            #region Pangle

            // https://developers.is.com/ironsource-mobile/unity/pangle-integration-guide/#step-5
            IronSource.Agent.setMetaData("Pangle_COPPA", "0");

            #endregion
        }
#endif


        private static void SetupPolicy()
        {
            #region AppLovin

           // https://dash.applovin.com/documentation/mediation/unity/getting-started/privacy
            MaxSdk.SetHasUserConsent(true);
            MaxSdk.SetIsAgeRestrictedUser(false);
            MaxSdk.SetDoNotSell(false);
            MaxSdk.SetLocationCollectionEnabled(true);

            #endregion

            // #region Meta
            //
            // // https://developers.facebook.com/docs/marketing-apis/data-processing-options
            // SetDataProcessingOptions(new string[] { });
            //
            // // https://developers.facebook.com/docs/app-events/guides/advertising-tracking-enabled
            // SetAdvertiserTrackingEnabled(true);
            //
            // #endregion
        }

#if UNITY_IOS
        [DllImport("__Internal")]
        private static extern void FBAdSettingsBridgeSetDataProcessingOptions(string[] dataProcessingOptions, int length);
        
        [DllImport("__Internal")] 
        private static extern void FBAdSettingsBridgeSetAdvertiserTrackingEnabled(bool advertiserTrackingEnabled);
#endif

        private static void SetDataProcessingOptions(string[] dataProcessingOptions)
        {
#if UNITY_ANDROID
            using var adSettings = new AndroidJavaClass("com.facebook.ads.AdSettings");
            adSettings.CallStatic("setDataProcessingOptions", (object)dataProcessingOptions);
#endif

#if UNITY_IOS
            FBAdSettingsBridgeSetDataProcessingOptions(dataProcessingOptions, dataProcessingOptions.Length);
#endif
        }

        private static void SetAdvertiserTrackingEnabled(bool advertiserTrackingEnabled)
        {
#if UNITY_IOS
            FBAdSettingsBridgeSetAdvertiserTrackingEnabled(advertiserTrackingEnabled);
#endif
        }
    }
}