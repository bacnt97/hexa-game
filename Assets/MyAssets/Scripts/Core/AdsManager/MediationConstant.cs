public class MediationConstant
{
    public static class IronSource
    {
        public const string AndroidKey = "";
        public const string IosKey = "";   
    }

    public static class Max
    {
        public const string SdkKey = "O0xNBWyPw60SbJm5tovIyW5gd6QLJvZuCMkF-qt18dDySdD6Sy6pLJJH3Dqv_l-47Lzzre9Qr-5cRuZEMAixPZ";
        public const string BannerStringId = "9253fddc38655510";
        public const string InterstitialStringId = "98202a3b7c95d845";
        public const string RewardedStringId = "94c63e8286970d03";
    }
}
