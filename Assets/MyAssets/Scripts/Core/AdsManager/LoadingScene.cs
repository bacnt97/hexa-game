/*
 * Created on 2022
 *
 * Copyright (c) 2022 dotmobstudio
 * Support : dotmobstudio@gmail.com
 */

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LoadingScene : MonoBehaviour
{
    private void Awake()
    {
        Application.targetFrameRate = 120;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.runInBackground = true;
    }

    void Start()
    {
        StartCoroutine(LoadMenuScene_IEnumerator());
    }

    public Image scale;
    public Text percent;

    public IEnumerator LoadMenuScene_IEnumerator()
    {
        percent.text = "";
        for (int i = 0; i < 100; i++)
        {
            percent.text = i + "%";
            scale.transform.localScale = new Vector3((float)(i / 100f), 1);
            yield return new WaitForSeconds(0.01f);
        }

        var loadScene = SceneManager.LoadSceneAsync("Game");
        while (!loadScene.isDone)
        {
            //percent.text = ((int)(loadScene.progress * 100)).ToString() +"%";
            yield return null;
        }

        try
        {
            percent.text = "100%";
            scale.transform.localScale = new Vector3(1, 1);
        }
        catch
        {
        }
    }

    bool isLoadMenu = false;
}