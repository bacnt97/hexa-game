using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using UnityEngine;
using UnityEngine.UI;

public enum HexaType
{
    WatchAds,
    Normal,
    Score,
}
public class HexaContainer : MonoBehaviour
{
    public HexaType type = HexaType.Normal;
    public GameObject hexaAds;
    public GameObject hexaNormal;
    public GameObject hexaScore;
    public Text scoreRequireTxt;
    private HexaAds hexaAdsLogic;

    public int scoreRequire;
    private void Start()
    {
        switch (type)
        {
            case HexaType.Normal:
                hexaAds.gameObject.SetActive(false);
                hexaNormal.gameObject.SetActive(true);
                hexaScore.gameObject.SetActive(false);
                break;
            case HexaType.WatchAds:
                hexaAds.gameObject.SetActive(true);
                hexaNormal.gameObject.SetActive(false);
                hexaScore.gameObject.SetActive(false);
                break;
            case HexaType.Score:
                scoreRequireTxt.text = scoreRequire.ToString();
                hexaAds.gameObject.SetActive(false);
                hexaNormal.gameObject.SetActive(false);
                hexaScore.gameObject.SetActive(true);
                break;
        }
        
        hexaAdsLogic = hexaAds.GetComponent<HexaAds>();
       
        if (hexaAdsLogic != null) return;
        hexaAdsLogic = hexaAds.AddComponent<HexaAds>();
        hexaAdsLogic.action = Click;
        GameManager.Instance.UpdateScore += UpdateScore;
    }

    private void UpdateScore()
    {
        if (type == HexaType.Score)
        {
            if (GameManager.Instance.slider.value >= scoreRequire)
            {
                hexaAds.gameObject.SetActive(false);
                hexaNormal.gameObject.SetActive(true);
                hexaScore.gameObject.SetActive(false);
            }
        }
    }
    private void Click()
    {
        if (hexaAds.activeInHierarchy)
        {
            AdsService.ShowRewardedVideo(Success);

            void Success(bool isSuccess)
            {
                if (isSuccess)
                {
                    hexaAds.gameObject.SetActive(false);
                    hexaNormal.gameObject.SetActive(true);
                }
            }
        }
    }
}
