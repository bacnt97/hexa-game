using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class TilesHolderSpawner : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public GameObject tilesHolderPrefab;
    private int spawTilesHolCount;

    private void Start()
    {
        SpawnTileHolders();

        Invoke(nameof(AddTileControllers), .3f);
    }

    private void AddTileControllers()
    {
        foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
        {
            if (!tilesHolder.enabled)
            {
                if (tilesHolder.GetComponent<TileController>() == null)
                    tilesHolder.gameObject.AddComponent<TileController>();
            }
        }
    }

    public async void RepsawnAll()
    {
        spawTilesHolCount = 0;

        SpawnTileHolders();

        U.TurnOffEventSystem();
        await Task.Delay(200);
        foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
        {
            if (!tilesHolder.enabled)
            {
                if (tilesHolder.GetComponent<TileController>() == null)
                    tilesHolder.gameObject.AddComponent<TileController>();
            }
        }

        U.TurnOnEventSystem();
    }

    public void DecreaseTileHolderCount()
    {
        spawTilesHolCount--;

        if (spawTilesHolCount == 0)
        {
            SpawnTileHolders();
        }
    }

    private async void SpawnTileHolders()
    {
        U.TurnOffEventSystem();
        var i = 0;
        foreach (Transform spaPoint in spawnPoints)
        {
            i++;
            GameObject obj = Instantiate(tilesHolderPrefab, spaPoint.position, Quaternion.identity);
            var pos = spaPoint.position;
            pos.x += 50;
            obj.transform.position = pos;
            obj.transform.DOMove(spaPoint.position, i * 0.2f);
            obj.name = "Name " + Time.timeScale + Random.Range(0, 100);
            spawTilesHolCount++;
            UpdateTileController(obj, spaPoint);
        }

        GetComponent<AudioSource>().Play();
        await Task.Delay(200);
        U.TurnOnEventSystem();
    }

    private async void UpdateTileController(GameObject obj, Transform spaPoint)
    {
        obj.GetComponent<TilesHolder>().initialPos = spaPoint.position;
    }
}