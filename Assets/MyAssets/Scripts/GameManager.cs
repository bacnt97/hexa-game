using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using Task = System.Threading.Tasks.Task;

public class BezierMove
{
    public void Run(Transform target, Transform projectile, float speed, Action success, Action fail)
    {
        var endPoint = target.position;
        var startPoint = projectile.position;

        Vector3 direction = (endPoint - startPoint).normalized;
        float distance = Vector3.Distance(startPoint, endPoint);
        var height = distance / 2f;
        var leftOrRight = Random.Range(0, 100) % 2 == 0 ? Vector3.left : Vector3.right;
        var controlPoint = startPoint + direction * distance * 0.5f + leftOrRight * height;
        var _timeToFire = distance / speed;

        //Tính vị trí chặn đầu (chưa chuẩn với một vài trường hợp - fix sau)
        //var finishTaget = _unitTarget.GetStat(RPGStatType.MoveSpeed) == 0 || _unitTarget.GetMovingTarget() == null ? endPoint :
        //    (Vector2)_unitTarget.UnitTransform.position +
        //    (Vector2)(_unitTarget.GetMovingTarget().position - _unitTarget.UnitTransform.position)
        //                  .normalized * _unitTarget.GetStat(RPGStatType.MoveSpeed) * _timeToFire;

        GameManager.Instance.StartCoroutine(CalculateBezierPoint(projectile, target, startPoint,
            controlPoint, endPoint, _timeToFire, speed, success, fail));
    }

    private IEnumerator CalculateBezierPoint(Transform projectile, Transform target, Vector3 p0, Vector3 p1,
        Vector3 p2,
        float _timeToFire, float speed,
        Action success, Action fail)
    {
        var oldPos = target.position;
        var isDie = false;
        float startTime = Time.time; // Thời gian bắt đầu di chuyển
        while (Time.time - startTime < _timeToFire)
        {
            if (projectile == null)
            {
                yield break;
            }

            if (projectile.gameObject.activeInHierarchy == false)
            {
                fail?.Invoke();
                yield break;
            }
            

            float t = (Time.time - startTime) / _timeToFire;
            var pos = CalculatorPos(p0, p1, oldPos, t);
            projectile.position = pos;
            // Calculate arrow direction and rotate it
            // Đợi cho đến khi kết thúc khung hình trước khi tiếp tục
            yield return new WaitForEndOfFrame();
        }

        if (!isDie)
            success?.Invoke();
        else
        {
            fail?.Invoke();
        }
    }

    public Vector3 CalculatorPos(Vector3 p0, Vector3 p1, Vector3 p2, float tParam)
    {
        float u = 1f - tParam;
        float tt = tParam * tParam;
        float uu = u * u;
        var pos = (uu * p0) + (2f * u * tParam * p1) + (tt * p2);
        return pos;
    }
}

public static class U
{
    private static EventSystem eventSystem;
    public static void TurnOffEventSystem()
    {
        if (eventSystem == null)
            eventSystem = EventSystem.current;
        eventSystem.enabled = false;
        GameManager.Instance.IsStop = true;
    }
    
    public static void TurnOnEventSystem()
    {
        if (eventSystem == null)
            eventSystem = EventSystem.current;
        eventSystem.enabled = true;
        GameManager.Instance.IsStop = false;
    }
}
public enum GameStatus
{
    Normal,
    Hammer,
    Move,
}
public class GameManager : MonoBehaviour
{
    public LevelManager levelManager;
    private GameStatus status = GameStatus.Normal;

    public GameObject bgSub;
    public GameObject uiMain;
    public GameObject uiSub;
    public GameObject hammer;
    public GameObject move;
    
    public Action ClickHammer;
    public Action ClickMove;
    public Action ClickNormal;

    private Tween tween;
    private Tween tween1;
    private void ClickMoveAction()
    {
        bgSub.SetActive(true);
        uiMain.gameObject.SetActive(true);
        uiSub.transform.localScale = Vector3.one * 4;
        
        hammer.gameObject.SetActive(false);
        move.gameObject.SetActive(true);
        
        tween = uiMain.transform.DOScale(Vector3.one * 4, 0.8f).OnComplete(() =>
        {
            uiMain.gameObject.SetActive(false);
        });
        tween1?.Complete();
        tween1 = uiSub.transform.DOScale(Vector3.one, 0.8f);
    }
    
    private void ClickHammerAction()
    {
        bgSub.SetActive(true);
        uiMain.gameObject.SetActive(true);
        uiSub.transform.localScale = Vector3.one * 4;
        
        hammer.gameObject.SetActive(true);
        move.gameObject.SetActive(false);
        
        tween = uiMain.transform.DOScale(Vector3.one * 4, 0.8f).OnComplete(() =>
        {
            uiMain.gameObject.SetActive(false);
        });
        tween1?.Complete();
        tween1 = uiSub.transform.DOScale(Vector3.one, 0.8f);
    }
    public async void ClickNormalAction()
    {
        bgSub.SetActive(false);
        status = GameStatus.Normal;
        
        tween?.Complete();
        uiMain.gameObject.SetActive(true);
        tween = uiMain.transform.DOScale(Vector3.one, 0.8f).OnComplete(() =>
        {
        });
        tween1?.Complete();
        tween1 = uiSub.transform.DOScale(Vector3.one * 4, 0.8f).OnComplete(() =>
        {
            hammer.gameObject.SetActive(false);
            move.gameObject.SetActive(false);
        });
        await Task.Delay(800);
        U.TurnOnEventSystem();
    }

    public bool IsStop;
    public GameStatus Status
    {
        set
        {
            status = value;
            switch (status)
            {
                case GameStatus.Hammer:
                    ClickHammer?.Invoke();
                    ClickHammerAction();
                    break;
                case GameStatus.Move:
                    ClickMove?.Invoke();
                    ClickMoveAction();
                    break;
                case GameStatus.Normal:
                    ClickNormal?.Invoke();
                    ClickNormalAction();
                    break;
            }
        }
        get => status;
    }
    public static GameManager Instance;
    
    public GameObject retryPanel, winPanel, startPanel, gamePanel;
    public Text levelText, sliderValueText, maxValueText;
    public Material selectedGroundMat, groundMat;
    public Slider slider;
    private int levelNo;
    public Color[] bgColors;
    public Material bgMat;
    public float incrementAmount; // Amount to increment the slider each time
    public float sliderIncreaseTime;
    private Coroutine smoothIncreaseCoroutine;
    private bool win;

    public GameObject levelPreview;

    public GameObject vfxPrefab;
    public List<GameObject> poolList = new List<GameObject>();
    public Transform parent;
    public Transform target;
    
    public GameObject vfxBoomPrefab;
    public List<GameObject> boomPoolList = new List<GameObject>();
    private GameObject GetBoomVfx()
    {
        foreach (var vfx in boomPoolList)
        {
            if (!vfx.activeInHierarchy)
            {
                vfx.gameObject.SetActive(true);
                return vfx;
            }
        }

        var obj = Instantiate(vfxBoomPrefab, parent);
        obj.gameObject.SetActive(true);
        boomPoolList.Add(obj);
        return obj;
    }
    private GameObject GetVfx()
    {
        foreach (var vfx in poolList)
        {
            if (!vfx.activeInHierarchy)
            {
                vfx.gameObject.SetActive(true);
                return vfx;
            }
        }

        var obj = Instantiate(vfxPrefab, parent);
        obj.gameObject.SetActive(true);
        poolList.Add(obj);
        return obj;
    }

    public void MoveVfx(Vector3 pos)
    {
        pos.y = target.transform.position.y;
        var vfx = GetVfx();
        vfx.transform.position = pos;

        BezierMove move = new BezierMove();
        move.Run(target.transform, vfx.transform, 12, () =>
        {
            var boom = GetBoomVfx();
            boom.transform.position = target.transform.position;
            FindObjectOfType<GameManager>().IncreaseLevelSlider();
            vfx.gameObject.SetActive(false);
        }, () =>
        {
            var boom = GetBoomVfx();
            boom.transform.position = vfx.transform.position;
            FindObjectOfType<GameManager>().IncreaseLevelSlider();
            vfx.gameObject.SetActive(false);
        });
    }

    public LevelConfig levelConfig;
    void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 120;
        levelNo = PlayerPrefs.GetInt("Level", 1);
        levelConfig = levelManager.GetLevelConfig();
        SetBgColor();
        SetSliderMaxValue();
        int maxSliderValue = 100 + (levelNo - 1) * 50;
        // Gradually increase the value of the slider over time

        sliderValueText.text = "0/" + maxSliderValue;
        levelText.text = "Level " + levelNo.ToString();

        Time.timeScale = 1;
        
        levelPreview.gameObject.SetActive(true);
    }

    private void SetBgColor()
    {
        int colorIndex = (levelNo - 1) / 5; // Calculate the color index based on level number
        int maxColorIndex = bgColors.Length - 1; // Maximum color index

        // Ensure colorIndex wraps around when it exceeds the maximum color index
        colorIndex = colorIndex % (maxColorIndex + 1);

        bgMat.color = bgColors[colorIndex];
    }

    private void SetSliderMaxValue()
    {
        int maxSliderValue = 100 + (levelNo - 1) * 10;
        slider.maxValue = maxSliderValue;

        //maxValueText.text = slider.maxValue.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            Time.timeScale = 1;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            PlayerPrefs.DeleteAll();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            PlayerPrefs.SetInt("Level", ++levelNo);
        }
    }

    public void Reload()
    {
        AudioManager.Instance.Play("Click");
        AdsService.ShowInterstitial(Success, onFail: Fail);

        void Success()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        void Fail()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public Action UpdateScore;
    public void StartGame()
    {
        AudioManager.Instance.Play("Click");
        // startPanel.SetActive(false);
        gamePanel.SetActive(true);
    }

    public void Win()
    {
        if (!win)
        {
            win = true;
            Invoke("InvokeWinGame", 1);
        }
    }

    public void Retry()
    {
        if (!win)
        {
            Invoke("InvokeRetryGame", 1);
        }
    }

    private void InvokeRetryGame()
    {
        Debug.Log("Retry");
        AudioManager.Instance.Play("Retry");
        retryPanel.SetActive(true);
    }

    private void InvokeWinGame()
    {
        Debug.Log("Win");
        AudioManager.Instance.Play("Win");
        winPanel.SetActive(true);
    }

    public void Next()
    {
        AudioManager.Instance.Play("Click");

        AdsService.ShowInterstitial(Success, onFail: Fail);

        void Success()
        {
            PlayerPrefs.SetInt("Level", ++levelNo);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        void Fail()
        {
            PlayerPrefs.SetInt("Level", ++levelNo);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void SoundToggle()
    {
        AudioManager.Instance.Play("Click");
        AudioManager.Instance.SoundToggle();
    }

    public void Home()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }

    public void OpenScene(int index)
    {
        AudioManager.Instance.Play("Click");
        SceneManager.LoadScene(index);
    }

    public void IncreaseLevelSlider()
    {
        // If there's a previous coroutine running, stop it
        if (smoothIncreaseCoroutine != null)
            StopCoroutine(smoothIncreaseCoroutine);

        // Determine the target value, ensuring it doesn't exceed the maximum value of 100
        float targetValue = Mathf.Min(slider.value + incrementAmount, slider.maxValue);

        // Start a new coroutine to smoothly increase the slider value
        smoothIncreaseCoroutine = StartCoroutine(SmoothIncreaseSlider(targetValue));
    }

    IEnumerator SmoothIncreaseSlider(float targetValue)
    {
        float timer = 0f;
        float startValue = slider.value;

        int maxSliderValue = levelConfig.score;
        // Gradually increase the value of the slider over time
        while (timer < sliderIncreaseTime)
        {
            timer += Time.deltaTime;
            slider.value = Mathf.Lerp(startValue, targetValue, timer / sliderIncreaseTime);

            int value = (int)slider.value;
            sliderValueText.text = value.ToString() + "/" + maxSliderValue;

            yield return null;
        }

        // Ensure the slider value reaches the exact target value
        slider.value = targetValue;

        if (slider.value == slider.maxValue)
            Win();

        // Reset the coroutine reference
        smoothIncreaseCoroutine = null;
        UpdateScore?.Invoke();
    }
}