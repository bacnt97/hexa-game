using System;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    public int colorId;
    public const float gotoTargetDuration = 0.6f;
    public const float rotateDuration = 0.6f;
    public const float scaleDuration = 0.3f;
    public Text text;
    public GameObject vfx;

    private void Awake()
    {
        text.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        text.gameObject.SetActive(false);
    }

    public void SetUpdateText(bool isLast, int count)
    {
        text.text = count.ToString();
        text.gameObject.SetActive(isLast);
    }
    
    public void HideText()
    {
        text.gameObject.SetActive(false);
    }
    public void GotoTarget(TilesHolder tilesHolder)
    {
        HideText();
        if (tilesHolder.transform.position.x < transform.position.x)
        {
            transform.DOLocalRotate(new Vector3(0, 0, 180), rotateDuration);
        }
        else if (tilesHolder.transform.position.x > transform.position.x)
            transform.DOLocalRotate(new Vector3(0, 0, -180), rotateDuration);
        else
        {
            if (tilesHolder.transform.position.z < transform.position.z)
            {
                transform.DOLocalRotate(new Vector3(-180, 0, 0), rotateDuration);
            }
            else
            {
                transform.DOLocalRotate(new Vector3(180, 0, 0), rotateDuration);
            }
        }

        transform.DOLocalJump(tilesHolder.spawnPoint.localPosition, 1.5f, 1, gotoTargetDuration).OnComplete(() =>
        {
            transform.eulerAngles = Vector3.zero;
        });

        tilesHolder.IncreaseYSpawnPointYPos();
        tilesHolder.tiles.Add(this);
        AudioManager.Instance.Play("Click");
    }

    public async void PlayDestroyAnim(bool isLast)
    {
        HideText();
        
        AudioManager.Instance.Play("TileDestroy");
        await Task.Delay(150);
        if (vfx == null) return;

        vfx.gameObject.SetActive(true);
        await Task.Delay(150);
        if (this == null) return;
        transform.DOScale(Vector3.zero, scaleDuration)
            .OnComplete(
                () =>
                {
                    if (isLast)
                    {
                        FindObjectOfType<GameManager>().MoveVfx(transform.position);
                    } 
                    Destroy(gameObject);
                });
    }
}