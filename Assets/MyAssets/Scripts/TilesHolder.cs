using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using RDG;
using Random = UnityEngine.Random;

public class TilesHolder : MonoBehaviour
{
    public int maxTiles;
    public List<Tile> tiles;
    public List<GameObject> tilesPrefabs;
    public Transform spawnPoint;
    public Vector3 initialPos;
    private float yIncVal;
    private int totalTilesCount;
    private int firstColorTilesCount;
    private int secondColorTilesCount;

    public Vector3 boxSize = new Vector3(1f, 1f, 1f);
    public LayerMask layerMask;
    [HideInInspector] public Collider[] colliders;
    public Transform groundTile;
    [HideInInspector] public bool isTilesMatched;
    public Collider collider;

    public void Start()
    {
        totalTilesCount = Random.Range(2, maxTiles + 1);
        firstColorTilesCount = Random.Range(0, totalTilesCount);
        SpawnTiles(firstColorTilesCount);
        secondColorTilesCount = totalTilesCount - firstColorTilesCount;
        SpawnTiles(secondColorTilesCount);
        this.enabled = false;

        SetUpdateText();
    }

    public void SetUpdateText()
    {
        if (tiles.Count == 0) return;

        var _count = 1;
        var colorId = tiles[^1].colorId;
        for (int i = 0; i < tiles.Count - 1; i++)
        {
            if (tiles[i].colorId == colorId) _count++;
        }

        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].SetUpdateText(i == tiles.Count - 1, _count);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        colliders = Physics.OverlapBox(transform.position, boxSize / 2f, Quaternion.identity, layerMask);
        colliders = FilterOutSelfColliders(colliders);
    }

    Collider[] FilterOutSelfColliders(Collider[] colliders)
    {
        // Filter out all colliders attached to the current GameObject
        return colliders.Where(c => c.transform != transform).ToArray();
    }

    private void SpawnTiles(int tilesToSpawn)
    {
        if (tilesToSpawn == 0)
            return;

        GameObject tilePrefab = tilesPrefabs[Random.Range(0, tilesPrefabs.Count)];

        for (int i = 0; i < tilesToSpawn; i++)
        {
            GameObject tile = Instantiate(tilePrefab, spawnPoint.position, Quaternion.identity, transform);
            var _tile = tile.GetComponent<Tile>();
            tiles.Add(_tile);

            IncreaseYSpawnPointYPos();
        }
    }

    public void IncreaseYSpawnPointYPos()
    {
        yIncVal += .25f;
        spawnPoint.localPosition = new Vector3(spawnPoint.localPosition.x, yIncVal, spawnPoint.localPosition.z);
    }

    public void DecreaseYSpawnPointYPos()
    {
        yIncVal -= .25f;
        spawnPoint.localPosition = new Vector3(spawnPoint.localPosition.x, yIncVal, spawnPoint.localPosition.z);
    }

    public int GetListSize()
    {
        return tiles.Count;
    }

    public const float throwTileDuration = 0.06f;

    public IEnumerator ThrowTiles(TilesHolder matchedTileHolder)
    {
        for (int i = tiles.Count - 1; i >= 0; i--)
        {
            if (tiles[i] == null) continue;
            if (matchedTileHolder.tiles.Count - 1 >= matchedTileHolder.tiles.Count) continue;
            if (tiles[i].colorId == matchedTileHolder.tiles[matchedTileHolder.tiles.Count - 1].colorId)
            {
                // Set other tileholder ColerId to My Color Id
                tiles[i].GotoTarget(matchedTileHolder);
                tiles[i].transform.parent = matchedTileHolder.transform;

                tiles.RemoveAt(i);

                if (tiles.Count == 0)
                {
                    groundTile.tag = "GroundTile";

                    GameManager gameManager = FindObjectOfType<GameManager>();
                    groundTile.GetComponent<MeshRenderer>().material = gameManager.groundMat;

                    gameObject.SetActive(false);
                    Destroy(gameObject, 2);
                    enabled = false;
                    yield return null;
                }

                DecreaseYSpawnPointYPos();
                yield return new WaitForSeconds(throwTileDuration);
            }
            else
            {
                break;
            }
        }
    }

    public int matchedCount;

    public const float delayMathcedTime = 0.04f;

    // Function to check if all tiles have the same index
    public async void CheckLimitReached()
    {
        if (tiles.Count < 10)
        {
            Debug.Log("Not Much Tiles");
            return;
        }

        int firstIndex = tiles[tiles.Count - 1].colorId;
        bool sameIndex = true;
        matchedCount = 0;

        for (int i = tiles.Count - 1; i >= 0; i--)
        {
            if (tiles[i].colorId != firstIndex)
            {
                sameIndex = false;
                break;
            }

            matchedCount++;
        }

        if (matchedCount >= 10)
        {
            FindObjectOfType<GameManager>().incrementAmount = matchedCount;

            isTilesMatched = true;
            for (int i = tiles.Count - 1; i >= 0; i--)
            {
                matchedCount--;
                tiles[i].PlayDestroyAnim(matchedCount == 0);
                tiles.RemoveAt(i);
                DecreaseYSpawnPointYPos();

                if (matchedCount == 0)
                    break;
                await Task.Delay((int)(delayMathcedTime * 1000));
            }

            Vibration.Vibrate(50);

            Debug.Log("SameColor");

            if (sameIndex)
            {
                Destroy(gameObject, 2f);
                groundTile.tag = "GroundTile";

                GameManager gameManager = FindObjectOfType<GameManager>();
                groundTile.GetComponent<MeshRenderer>().material = gameManager.groundMat;
            }
        }
        else
        {
            Debug.Log("NotSameColor");
        }
    }

    public async void DestroyHammer()
    {
        var check = 0;
        try
        {
            isTilesMatched = true;

            check = 1;
            for (int i = tiles.Count - 1; i >= 0; i--)
            {
                check = 2;
                tiles[i].PlayDestroyAnim(false);

                check = 3;
                tiles.RemoveAt(i);
                check = 4;

                check = 5;
                DecreaseYSpawnPointYPos();
                check = 6;
                await Task.Delay((int)(delayMathcedTime * 1000));
            }

            check = 7;
            Vibration.Vibrate(50);

            check = 8;
            if (gameObject != null)
                Destroy(gameObject, 2f);
            check = 9;
            groundTile.tag = "GroundTile";
            check = 10;
            GameManager gameManager = FindObjectOfType<GameManager>();
            check = 11;
            groundTile.GetComponent<MeshRenderer>().material = gameManager.groundMat;
            check = 12;
        }
        catch (Exception e)
        {
            Debug.LogError("Error: " + check);
            Debug.LogError(e);
        }
    }
}