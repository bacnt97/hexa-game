using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class MoveAdsLogic : MonoBehaviour
{
    public Button btn;

    public GameObject ads;
    public GameObject free;

    public Text count;

    public List<TileController> caches;

    private void Awake()
    {
        btn.onClick.AddListener(OnClick);

        UpdateView();
    }

    private void Start()
    {
        GameManager.Instance.ClickNormal = Remove;
    }

    private void UpdateView()
    {
        var moveCount = PlayerPrefs.GetInt("MoveCount", 1);
        if (moveCount <= 0)
        {
            PlayerPrefs.SetInt("MoveCount", 0);
            ads.gameObject.SetActive(true);
            free.gameObject.SetActive(false);
        }
        else
        {
            free.gameObject.SetActive(true);
            ads.gameObject.SetActive(false);
        }

        count.text = moveCount.ToString();
    }

    private void OnClick()
    {
        if (FindObjectOfType<TilesHolderChecker>().isCheck) return;
        caches.Clear();
        var moveCount = PlayerPrefs.GetInt("MoveCount", 1);
        if (moveCount == 0)
        {
            AdsService.ShowRewardedVideo(OnSuccess);

            void OnSuccess(bool isSuccess)
            {
                if (isSuccess)
                {
                    PlayerPrefs.SetInt("MoveCount", 1);
                    GameManager.Instance.Status = GameStatus.Move;

                    foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
                    {
                        if (tilesHolder.enabled)
                        {
                            if (tilesHolder.gameObject.GetComponent<TileController>() != null)
                            {
                                if (!caches.Contains(tilesHolder.gameObject.GetComponent<TileController>()))
                                {
                                    caches.Add(tilesHolder.gameObject.GetComponent<TileController>());
                                }
                            }
                            else
                                caches.Add(tilesHolder.gameObject.AddComponent<TileController>());

                            tilesHolder.enabled = false;
                        }
                    }

                    UpdateView();
                }
            }
        }
        else
        {
            GameManager.Instance.Status = GameStatus.Move;

            foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
            {
                if (tilesHolder.enabled)
                {
                    if (tilesHolder.gameObject.GetComponent<TileController>() == null)
                    {
                        tilesHolder.gameObject.AddComponent<TileController>();
                    }
                    else
                    {
                        tilesHolder.gameObject.GetComponent<TileController>().enabled = true;
                    }
                    tilesHolder.enabled = false;
                }
            }
        }
    }

    private bool touching;

    public virtual void Update()
    {
        if (GameManager.Instance.Status != GameStatus.Move) return;
        if (touching)
        {
            TouchHold(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(0) && !touching)
        {
            touching = true;
            TouchBegin(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            touching = false;
            TouchEnd(Input.mousePosition);
        }

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touching = true;
                TouchBegin(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                TouchHold(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                touching = false;
                TouchEnd(Input.GetTouch(0).position);
            }
        }
    }

    protected virtual GameObject ObjectClicked(Vector3 screenPosition)
    {
        // //Converting Mouse Pos to 2D (vector2) World Pos
        // Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPosition);
        // Vector2 rayPos = new Vector2(worldPos.x, worldPos.y);
        // int layerMask = 1 << LayerMask.NameToLayer("GroundTile");
        // RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f, layerMask);
        // if (hit.transform != null)
        // {
        //     if (hit.transform.gameObject.tag.Equals("GroundTile"))
        //         return hit.transform.gameObject;
        // }
        //Converting Mouse Pos to 2D (vector2) World Pos
        var worldPos = Camera.main.ScreenPointToRay(screenPosition);
        var obj = Physics.RaycastAll(worldPos, float.MaxValue);
        if (obj is { Length: > 0 })
        {
            GameObject min = null;
            var distance = float.MaxValue;
            foreach (var hit in obj)
            {
                if (hit.transform.gameObject.tag.Equals("TileHolder"))
                {
                    return hit.transform.gameObject;
                }
            }
        }

        return null;
    }

    private GameObject cache;

    private void TouchBegin(Vector3 position)
    {
        cache = ObjectClicked(position);
        if (cache != null)
        {
            var tile = cache.GetComponent<TileController>();
            if (tile == null)
            {
                cache = null;
                return;
            }

            if (tile.GetComponent<TilesHolder>().groundTile == null)
            {
                cache = null;
                return;
            }

            tile.OnMouseDown();
            UpdateView();
        }

        touching = true;
    }

    private async void TouchEnd(Vector3 position)
    {
        touching = false;
        if (cache == null)
        {
            UpdateView();
            return;
        }

        if (cache.GetComponent<TilesHolder>() == null)
        {
            cache = null;
            return;
        }

        GameManager.Instance.Status = GameStatus.Normal;
        cache = null;
        PlayerPrefs.SetInt("MoveCount", PlayerPrefs.GetInt("MoveCount", 1) - 1);
        UpdateView();
    }

    private async void Remove()
    {
        foreach (var _cache in caches)
        {
            if (_cache == null) continue;
            _cache.GetComponent<TilesHolder>().enabled = true;
            if (cache != _cache.gameObject)
                Destroy(_cache);
            else
            {
                _cache.isMove = true;
            }
        }

        caches.Clear();
        U.TurnOffEventSystem();
    }

    private void TouchHold(Vector3 mousePosition)
    {
    }
}