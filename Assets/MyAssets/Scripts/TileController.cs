using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class TileController : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;
    public List<Transform> targetList;
    public Transform targetPos;
    private bool isMouseDown;
    private Material selectedGroundMat, groundMat;

    private MeshRenderer meshCurrent;

    public bool isMove;
    // private void OnDestroy()
    // {
    //     GetComponent<TilesHolder>().collider.enabled = false;
    // }
    //
    // private void OnEnable()
    // {
    //     GetComponent<TilesHolder>().collider.enabled = true;
    // }
    private async void Start()
    {
        targetList = new();
        GameManager gameManager = FindObjectOfType<GameManager>();
        selectedGroundMat = gameManager.selectedGroundMat;
        groundMat = gameManager.groundMat;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;

        // z coordinate of game object on screen
        mousePoint.z = mZCoord;

        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void ResetTarget()
    {
        targetPos = null;
        targetList.Clear();
    }

    private Vector3 movePos;

    public void OnMouseDown()
    {
        if (isMove) return;
        
        if (GameManager.Instance.IsStop) return;
        
        if (GameManager.Instance.Status == GameStatus.Hammer) return;
        if (GameManager.Instance.Status == GameStatus.Move)
        {
            if (transform.GetComponent<TilesHolder>().groundTile == null) return;
        }

        isMouseDown = true;
        movePos = transform.position;
        mZCoord = Camera.main.WorldToScreenPoint(
            gameObject.transform.position).z;

        // Store offset = gameobject world pos - mouse world pos
        mOffset = transform.position - GetMouseAsWorldPoint();
        //GetComponent<TilesHolder>().collider.enabled = false;
    }

    private GameObject current;

    void OnMouseDrag()
    {
        if (isMove) return;
        if (GameManager.Instance.IsStop) return;
        if (GameManager.Instance.Status == GameStatus.Hammer) return;

        if (!isMouseDown)
            return;
        var pos = GetMouseAsWorldPoint() + mOffset;
        if (pos.y < 1) pos.y = 1;
        transform.position = new Vector3(pos.x, pos.y, pos.z);
    }

    public bool isSuccess;
    private void OnMouseUp()
    {
        if (isMove) return;
        if (GameManager.Instance.IsStop) return;
        
        if (GameManager.Instance.Status == GameStatus.Hammer)
        {
            return;
        }

        if (!isMouseDown) return;

        isMouseDown = false;
        if (targetPos == null)
        {
            // GetComponent<TilesHolder>().collider.enabled = true;
            if (GameManager.Instance.Status == GameStatus.Move)
            {
                transform.position = movePos;
            }
            else
            {
                transform.position = transform.GetComponent<TilesHolder>().initialPos;
            }

            isSuccess = false;
            return;
        }

        if (GameManager.Instance.Status == GameStatus.Move)
        {
            transform.GetComponent<TilesHolder>().groundTile.tag = "GroundTile";

            GameManager gameManager = FindObjectOfType<GameManager>();
            transform.GetComponent<TilesHolder>().groundTile.GetComponent<MeshRenderer>().material =
                gameManager.groundMat;
        }

        foreach (TileController tileController in FindObjectsOfType<TileController>())
        {
            if (tileController != this)
            {
                Destroy(tileController);
            }
        }

        AudioManager.Instance.Play("TilePlace");

        Vector3 pos = targetPos.position;
        pos.y += .25f;
        transform.position = pos;
        targetPos.tag = "Untagged";
        GetComponent<TilesHolder>().enabled = true;
        GetComponent<TilesHolder>().groundTile = targetPos;
        gameObject.layer = 15;

        InvokeCallTilesHolder();
        if (GameManager.Instance.Status == GameStatus.Normal)
            FindObjectOfType<TilesHolderSpawner>().DecreaseTileHolderCount();
        isSuccess = true;
    }

    private async void InvokeCallTilesHolder()
    {
        await Task.Delay(100);
        if (this == null) return;
        
        FindObjectOfType<TilesHolderChecker>().CheckOneTilesHolder(GetComponent<TilesHolder>());
        // GetComponent<TilesHolder>().collider.enabled = false;
        Destroy(this);
    }

    private void UpdateTarget()
    {
        if (targetList == null || targetList.Count == 0)
        {
            targetPos = null;
            return;
        }

        var isMin = float.MaxValue;
        Transform min = null;
        for (int i = 0; i < targetList.Count; i++)
        {
            var distance = (targetList[i].position - transform.position).magnitude;
            if (isMin > distance)
            {
                isMin = distance;
                min = targetList[i];
            }
        }

        if (min == null)
        {
            targetPos = null;
            return;
        }

        if (meshCurrent != null)
        {
            meshCurrent.material = groundMat;
        }

        targetPos = min.transform;
        meshCurrent = min.GetComponent<MeshRenderer>();
        meshCurrent.material = selectedGroundMat;
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.CompareTag("GroundTile") && isMouseDown)
    //     {
    //         if (!targetList.Contains(other.transform))
    //         {
    //             targetList.Add(other.transform);
    //             UpdateTarget();
    //         }
    //     }
    // }
    //
    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.CompareTag("GroundTile"))
    //     {
    //         // targetList.Remove(other.transform);
    //         // other.GetComponent<MeshRenderer>().material = groundMat;
    //         UpdateTarget();
    //     }
    // }

    protected virtual GameObject ObjectClicked(Vector3 screenPosition)
    {
        // //Converting Mouse Pos to 2D (vector2) World Pos
        // Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPosition);
        // Vector2 rayPos = new Vector2(worldPos.x, worldPos.y);
        // int layerMask = 1 << LayerMask.NameToLayer("GroundTile");
        // RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f, layerMask);
        // if (hit.transform != null)
        // {
        //     if (hit.transform.gameObject.tag.Equals("GroundTile"))
        //         return hit.transform.gameObject;
        // }
        //Converting Mouse Pos to 2D (vector2) World Pos
        var worldPos = Camera.main.ScreenPointToRay(screenPosition);
        var obj = Physics.RaycastAll(worldPos, float.MaxValue);
        if (obj is { Length: > 0 })
        {
            GameObject min = null;
            var distance = float.MaxValue;
            foreach (var hit in obj)
            {
                if (hit.transform.gameObject.tag.Equals("GroundTile"))
                {
                    return hit.transform.gameObject;
                }
            }
        }

        return null;
    }

    private bool touching;

    public virtual void Update()
    {
        if (GameManager.Instance.Status == GameStatus.Hammer) return;
        if (!isMouseDown) return;
        //if (SN.InternetRequire.gameObject.activeSelf)
        //{
        //    return;
        //}
        //if (isPause) return;

        if (touching)
        {
            TouchHold(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(0) && !touching)
        {
            touching = true;
            TouchBegin(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            touching = false;
            TouchEnd(Input.mousePosition);
        }

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touching = true;
                TouchBegin(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                TouchHold(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                touching = false;
                TouchEnd(Input.GetTouch(0).position);
            }
        }
    }

    private void TouchBegin(Vector3 position)
    {
        touching = true;
    }

    private void TouchEnd(Vector3 position)
    {
        touching = false;
        if (meshCurrent != null)
        {
            meshCurrent.material = groundMat;
        }

        if (current != null)
            targetPos = current.transform;
        else
        {
            targetPos = null;
        }
    }

    private void TouchHold(Vector3 mousePosition)
    {
        current = ObjectClicked(Input.mousePosition);
        if (current != null)
        {
            if (meshCurrent != null)
            {
                meshCurrent.material = groundMat;
            }

            targetPos = current.transform;
            meshCurrent = current.GetComponent<MeshRenderer>();
            meshCurrent.material = selectedGroundMat;
        }
        else
        {
            if (meshCurrent != null)
            {
                meshCurrent.material = groundMat;
            }

            targetPos = null;
        }
    }
}