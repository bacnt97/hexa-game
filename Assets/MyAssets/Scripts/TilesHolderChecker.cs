using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesHolderChecker : MonoBehaviour
{
    private int matchedTilesCount = 0;
    private TilesHolder[] tilesHolders;
    public bool runCheckAllTilesHol;
    public float speed;
    public bool tilesMatched;
    public bool isCheck;
    public void CheckAllTilesHolder()
    {
        FindFillTilesHolderArray();
        StartCoroutine(CoroutineCheckAllTilesHolder());
    }

    private void FindFillTilesHolderArray()
    {
        List<TilesHolder> tempTilesHolders = new List<TilesHolder>();

        foreach (var tileHol in FindObjectsOfType<TilesHolder>())
        {
            if (tileHol.enabled)
                tempTilesHolders.Add(tileHol);
        }

        tilesHolders = tempTilesHolders.ToArray();

        // Sort the array based on list sizes
        Array.Sort(tilesHolders, new TilesHolderComparer());
    }

    IEnumerator CoroutineCheckAllTilesHolder()
    {
        isCheck = true;
        foreach (TilesHolder tilesHol in tilesHolders)
        {
            if (!tilesHol)
                continue;

            if (tilesHol.tiles.Count == 0)
                continue;

            foreach (Collider col in tilesHol.colliders)
            {
                if (col == null) continue;
                var tileHolder = col.GetComponent<TilesHolder>();
                if (tileHolder == null) continue;
                if (tileHolder.tiles.Count == 0) continue;
                if (tilesHol.tiles.Count > 0 && tileHolder.tiles.Count > 0)
                {
                    if (tilesHol.tiles[^1].colorId == tileHolder
                            .tiles[^1].colorId)
                    {
                        matchedTilesCount++;
                        yield return tileHolder.ThrowTiles(tilesHol);
                        yield return new WaitForSeconds(0.3f);
                    }
                }
            }
        }

        Debug.Log("Finished processing all objects");

        if (matchedTilesCount > 0)
        {
            matchedTilesCount = 0;
            runCheckAllTilesHol = true;
        }
        else
        {
            var waitTime = 0f;
            foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
            {
                tilesHolder.CheckLimitReached();

                if (tilesHolder.isTilesMatched)
                {
                    tilesHolder.isTilesMatched = false;
                    tilesMatched = true;
                    waitTime += tilesHolder.matchedCount * TilesHolder.delayMathcedTime;
                }
            }

            if (tilesMatched)
            {
                yield return new WaitForSeconds(waitTime + 1.2f);

                tilesMatched = false;
                runCheckAllTilesHol = true;
            }
            else
            {
                isCheck = false;
                foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
                {
                    if (!tilesHolder.enabled)
                    {
                        if (tilesHolder.GetComponent<TileController>() == null)
                            tilesHolder.gameObject.AddComponent<TileController>();
                    }
                }
            }
        }

        var tileHolders = FindObjectsOfType<TilesHolder>();
        foreach (TilesHolder tilesHolder in tileHolders)
        {
            tilesHolder.SetUpdateText();
        }

        tilesHolders = null;
        CheckEmptyGround();
        yield return null;
    }

    private void CheckEmptyGround()
    {
        if (!GameObject.FindGameObjectWithTag("GroundTile"))
        {
            FindObjectOfType<GameManager>().Retry();
        }
    }

    public void CheckOneTilesHolder(TilesHolder _tilesHolder)
    {
        StartCoroutine(CoroutineCheckOneTilesHolder(_tilesHolder));
    }


    IEnumerator CoroutineCheckOneTilesHolder(TilesHolder _tilesHolder)
    {
        isCheck = true;
        if (_tilesHolder == null) yield return null;

        foreach (Collider col in _tilesHolder.colliders)
        {
            if (col == null) continue;
            var holder = col.GetComponent<TilesHolder>();
            if (_tilesHolder == null || _tilesHolder.tiles.Count == 0 || holder == null || holder.tiles.Count == 0)
            {
                continue;
            }


            if (_tilesHolder.tiles[^1].colorId == holder
                    .tiles[^1].colorId)
            {
                matchedTilesCount++;
                yield return holder.ThrowTiles(_tilesHolder);
                yield return new WaitForSeconds(0.3f);
            }
        }

        Debug.Log("Finished processing all objects");

        if (matchedTilesCount > 0)
        {
            matchedTilesCount = 0;
            runCheckAllTilesHol = true;
        }
        else
        {
            isCheck = false;
            foreach (TilesHolder tilesHolder in FindObjectsOfType<TilesHolder>())
            {
                if (!tilesHolder.enabled)
                {
                    if (tilesHolder.GetComponent<TileController>() == null)
                        tilesHolder.gameObject.AddComponent<TileController>();
                }
            }

            CheckEmptyGround();
        }

        yield return null;
    }


    private void Update()
    {
        if (runCheckAllTilesHol)
        {
            runCheckAllTilesHol = false;
            CheckAllTilesHolder();
        }
    }
}

public class TilesHolderComparer : IComparer<TilesHolder>
{
    public int Compare(TilesHolder x, TilesHolder y)
    {
        // Compare based on list size
        return x.GetListSize().CompareTo(y.GetListSize());
    }
}