using UnityEngine;

[System.Serializable]
public class LevelCollection : ScriptableObject
{
    public LevelConfig[] dataGroups;
}

[System.Serializable]
public class LevelConfig
{
    public int level;
    public int score;
    public int shape;
    public int colorLimit;
    public int ra;
    public int score1;
    public int score2;
    public int score3;
    public int score4;
    public int score5;
    public int score6;
    public int score7;
    public int score8;
    public int score9;
    public int score10;
}