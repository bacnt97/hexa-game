using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private GameObject currenShape;
    public GameObject shapeDefault;
    public LevelCollection levelCollection;

    private void Awake()
    {
        SpawnDefault();
    }

    public LevelConfig GetLevelConfig()
    {
        if (levelCollection == null) return null;
        var level = PlayerPrefs.GetInt("Level", 1);
        
        for (int i = 0; i < levelCollection.dataGroups.Length; i++ )
        {
            if (level % levelCollection.dataGroups.Length == i) return levelCollection.dataGroups[i];
        }

        return levelCollection.dataGroups[^1];
    }

    private async void SpawnDefault()
    {
        var levelConfig = GetLevelConfig();
        var newShape = shapeDefault;
        if (levelConfig != null)
        {
            var shapePath = "shape_" + levelConfig.shape;
            newShape = Resources.Load<GameObject>(shapePath);
            if (newShape == null) newShape = shapeDefault;
        }

        currenShape = Instantiate(newShape);
    }

    [Button("Next Level")]
    public async void NextLevel()
    {
        PlayerPrefs.SetInt("Level",  PlayerPrefs.GetInt("Level", 1) + 1);
        SceneManager.LoadScene("Game");
    }
    
    [Button("Spawn Shape")]
    public async void Spawn(int shapeId)
    {
        if (currenShape != null)
        {
            Destroy(currenShape);
        }

        await Task.Delay(50);

        var shapePath = "shape_" + shapeId;
        var newShape = Resources.Load<GameObject>(shapePath);
        if (newShape == null) newShape = shapeDefault;
        currenShape = Instantiate(newShape);
    }
}