using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ShuffleLogic : MonoBehaviour
{
    public Button btn;

    public GameObject ads;
    public GameObject free;
    public GameObject shuffleLogic;
    public Text count;

    private void Awake()
    {
        btn.onClick.AddListener(OnClick);

        UpdateView();
    }

    private void UpdateView()
    {
        var shuffleCount = PlayerPrefs.GetInt("ShuffleCount", 1);
        if (shuffleCount <= 0)
        {
            PlayerPrefs.SetInt("ShuffleCount", 0);
            ads.gameObject.SetActive(true);
            free.gameObject.SetActive(false);
        }
        else
        {
            free.gameObject.SetActive(true);
            ads.gameObject.SetActive(false);
        }

        count.text = shuffleCount.ToString();
    }

    private async void OnClick()
    {
        if (FindObjectOfType<TilesHolderChecker>().isCheck) return;
        
        var shuffleCount = PlayerPrefs.GetInt("ShuffleCount", 1);
        if (shuffleCount <= 0)
        {
            AdsService.ShowRewardedVideo(OnSuccess);

            async void OnSuccess(bool isSuccess)
            {
                if (isSuccess)
                {
                    shuffleLogic.gameObject.SetActive(true);
                    await Task.Delay(210);
                    shuffleLogic.gameObject.SetActive(false);
                    FindObjectOfType<TilesHolderSpawner>().RepsawnAll();
                }

                UpdateView();
            }
        }
        else
        {
            PlayerPrefs.SetInt("ShuffleCount", PlayerPrefs.GetInt("ShuffleCount") - 1);
            UpdateView();
            
            shuffleLogic.gameObject.SetActive(true);
            await Task.Delay(210);
            shuffleLogic.gameObject.SetActive(false);
            FindObjectOfType<TilesHolderSpawner>().RepsawnAll();
        }
    }
}