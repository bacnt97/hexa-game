using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HammerLogic : MonoBehaviour
{
    public Button btn;

    public GameObject ads;
    public GameObject free;
    
    public Text count;
    
    private void Awake()
    {
        btn.onClick.AddListener(OnClick);

        UpdateView();
    }

    private void UpdateView()
    {
        var hammerCount = PlayerPrefs.GetInt("HammerCount", 1);
        if (hammerCount <= 0)
        {
            PlayerPrefs.SetInt("HammerCount", 0);
            ads.gameObject.SetActive(true);
            free.gameObject.SetActive(false);
        }
        else
        {
            free.gameObject.SetActive(true);
            ads.gameObject.SetActive(false);
        }

        count.text = hammerCount.ToString();
    }
    private void OnClick()
    {
        if (FindObjectOfType<TilesHolderChecker>().isCheck) return;
        
        var hammerCount = PlayerPrefs.GetInt("HammerCount", 1);
        if (hammerCount == 0)
        {
            AdsService.ShowRewardedVideo(OnSuccess);

            void OnSuccess(bool isSuccess)
            {
                if (isSuccess)
                {
                    PlayerPrefs.SetInt("HammerCount", 1);
                    GameManager.Instance.Status = GameStatus.Hammer;
                    UpdateView();
                }
            }
        }
        else
        {
            GameManager.Instance.Status = GameStatus.Hammer;
        }
    }

    private bool touching;

    public virtual void Update()
    {
        if (GameManager.Instance.Status != GameStatus.Hammer) return;
        if (touching)
        {
            TouchHold(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(0) && !touching)
        {
            touching = true;
            TouchBegin(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            touching = false;
            TouchEnd(Input.mousePosition);
        }

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touching = true;
                TouchBegin(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                TouchHold(Input.GetTouch(0).position);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                touching = false;
                TouchEnd(Input.GetTouch(0).position);
            }
        }
    }

    protected virtual GameObject ObjectClicked(Vector3 screenPosition)
    {
        // //Converting Mouse Pos to 2D (vector2) World Pos
        // Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPosition);
        // Vector2 rayPos = new Vector2(worldPos.x, worldPos.y);
        // int layerMask = 1 << LayerMask.NameToLayer("GroundTile");
        // RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f, layerMask);
        // if (hit.transform != null)
        // {
        //     if (hit.transform.gameObject.tag.Equals("GroundTile"))
        //         return hit.transform.gameObject;
        // }
        //Converting Mouse Pos to 2D (vector2) World Pos
        var worldPos = Camera.main.ScreenPointToRay(screenPosition);
        var obj = Physics.RaycastAll(worldPos, float.MaxValue);
        if (obj is { Length: > 0 })
        {
            GameObject min = null;
            var distance = float.MaxValue;
            foreach (var hit in obj)
            {
                if (hit.transform.gameObject.tag.Equals("TileHolder"))
                {
                    return hit.transform.gameObject;
                }
            }
        }

        return null;
    }

    private void TouchBegin(Vector3 position)
    {
        touching = true;
    }

    private void TouchEnd(Vector3 position)
    {
        touching = false;
        var obj = ObjectClicked(position);
        if (obj != null)
            if (obj.GetComponent<TileController>() == null && obj.GetComponent<TilesHolder>() != null)
            {
                obj.GetComponent<TilesHolder>().DestroyHammer();
                PlayerPrefs.SetInt("HammerCount", PlayerPrefs.GetInt("HammerCount", 1) - 1);
                GameManager.Instance.Status = GameStatus.Normal;
            }
        UpdateView();
    }

    private void TouchHold(Vector3 mousePosition)
    {
    }
}