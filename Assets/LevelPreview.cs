using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Task = System.Threading.Tasks.Task;

public class LevelPreview : MonoBehaviour
{
    public Text valueText;
    public Text levelText;
    public Image bg;

    public float fade;

    public GameObject[] showingObjects;

    private async void Awake()
    {
        bg.DOFade(fade, 1);

        var levelNo = PlayerPrefs.GetInt("Level", 1);

        int maxSliderValue = 100 + (levelNo - 1) * 50;
        // Gradually increase the value of the slider over time

        valueText.text = maxSliderValue.ToString();
        levelText.text = "Level "+levelNo.ToString();
        foreach (var obj in showingObjects)
        {
            obj.transform.localScale = Vector3.zero;
        }

        foreach (var obj in showingObjects)
        {
            await Task.Delay(100);
            obj.transform.DOScale(Vector3.one, 0.1f);
        }
    }

    private async void OnEnable()
    {
        await Task.Delay(2000);

        bg.DOFade(0, 0.5f);

        foreach (var obj in showingObjects)
        {
            obj.transform.DOScale(Vector3.zero, 0.1f);
        }
        
        await Task.Delay(800);
        if (this == null || gameObject == null) return;
        gameObject.SetActive(false);
    }
}