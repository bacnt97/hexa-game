using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScaleHelper : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var btn = transform.GetComponent<Button>();
        if (btn != null)
        {
            btn.onClick.AddListener(() =>
            {
                transform.DOScale(Vector3.one * 1.1f, 0.1f).OnComplete(() =>
                {
                    transform.DOScale(Vector3.one, 0.1f);
                });
            });
        }
    }

    // Update is called once per fram
}
